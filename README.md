# Convert CSV to JSON script
## Description
This script converts a CSV file into a JSON file

## How to use the script
### Clone the project with HTTPS
    $ git clone https://gitlab.com/uvic-arcsoft/convert-csv-to-json.git

### Run the script:
#### With your own test file:
    $ cd convert-csv-to-json    # Move to the project directory
    $ python3 script.py <input_file_name> <output_file_name>
Note that `<input_file_name>` should be a **CSV** file and `<output_file_name>` should be a **JSON** file. If `<output_file_name>` is left empty, `output.json` will be the default output file's name.
#### With the provided test file:
    $ cd convert-csv-to-json    # Move to the project directory
    $ python3 script.py test.csv test.json  # Test with 'test.csv' as the input file and 'test.json' as the output file
    $ python3 script.py test.csv    # Test with 'test.csv' as the input file and 'output.json' as the output file (the default value)
