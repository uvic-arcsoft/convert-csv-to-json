import csv
import json
from datetime import datetime, timezone
import sys

# JSON item represents a row of the csv file
class JSONItem:
    def __init__(self):
        self.dict = {}

    # Recursively set key of dictionary to a value
    def populate_data(self, dict, keys, val):
        '''
        '''
        # Fixed email address for users
        if keys == ["user", "email"]:
            if val == "":
                dict["user"] = {"email": "systemsrcs@uvic.ca"}
            else:
                dict["user"] = {"email": val}
            return

        key = keys[0]

        # Ignore if value is empty
        if val == "" and key != '@timestamp':
            return

        # Set value if there is one key
        if len(keys) == 1:
            if key == '@timestamp':
                dict[key] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S+00:00')
            else:
                dict[key] = val
            return
        # Set a dictionary if there are more sub keys
        else:
            if key not in dict:
                dict[key] = {}
            self.populate_data(dict[key], keys[1:], val)
    


# Function to convert a CSV to JSON
# Takes the file paths as arguments
def make_json(csvFilePath, jsonFilePath):
     
    # create a list
    data = []

    try:
        # Open a csv reader called DictReader
        with open(csvFilePath, encoding='utf-8') as csvf:
            csvReader = csv.DictReader(csvf)

            # Convert each row into a dictionary
            for row in csvReader:
                # Convert each row in to a nested directionary format
                json_item = JSONItem()
                for (first_key, value) in row.items():
                    # Get list of sub keys
                    sub_keys = first_key.split(".")
                    json_item.populate_data(json_item.dict, sub_keys, value)
                
                # Add to the list that contains all JSON items
                data.append(json_item.dict)
    except FileNotFoundError:
        print("Input file not found: " + f"'{csvFilePath}'")

    # Open a json writer, and use the json.dumps()
    # function to dump data
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=2))

def main():
    
    # Decide the two file paths according to your
    # computer system
    csvFilePath = sys.argv[1]
    jsonFilePath = sys.argv[2] if len(sys.argv) >= 3 else "output.json"

    print("Input file: " + csvFilePath)
    print("Output file: " + jsonFilePath)

    # Call the make_json function
    make_json(csvFilePath, jsonFilePath)


if __name__ == "__main__": 
    main()